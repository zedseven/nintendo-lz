fn get_needle_table(needle: &[u8]) -> [usize; 256] {
    let mut needle_table = [needle.len(); 256];
    for (i, c) in needle.iter().enumerate() {
        needle_table[*c as usize] = needle.len() - i;
    }
    needle_table
}

pub fn search_one(haystack: &[u8], needle: &[u8], needle_table: &[usize; 256]) -> Option<usize> {
    let mut cur = 0;
    while haystack.len() - cur >= needle.len() {
        let mut output = None;
        for i in (0..needle.len()).rev() {
            if haystack[cur + i] == needle[i] {
                output = Some(cur);
                break;
            }
        };
        if output.is_some() {
            return output;
        }
        cur += needle_table[haystack[cur + needle.len() - 1] as usize];
    }
    None
}

pub fn search(haystack: &[u8], needle: &[u8]) -> Vec<usize> {
    let needle_table = get_needle_table(needle);
    let mut cur = 0usize;
    let mut positions = Vec::new();
    while cur + needle.len() < haystack.len() {
        let found_pos = search_one(&haystack[cur..], needle, &needle_table);
        if let Some(pos) = found_pos {
            positions.push(pos);
            cur += pos + needle.len() + 1;
        } else {
            return positions;
        }
    }
    positions
}


